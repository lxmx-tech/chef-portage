include_recipe 'portage'

portage_use "sys-kernel/genkernel -cryptsetup"

portage_use "sys-kernel/gentoo-sources symlink"
portage_use "sys-kernel/gentoo-sources symlink" do
  action :remove
end

portage_mask ">=app-admin/chef-0.10.24"

portage_mask ">=net-libs/nodejs-0.9.0"
portage_mask ">=net-libs/nodejs-0.9.0" do
  action :remove
end

portage_keywords "dev-db/mysql ~amd64"

portage_keywords "dev-vcs/git ~amd64"
portage_keywords "dev-vcs/git ~amd64" do
  action :remove
end

portage_make_conf_entry "FEATURES" do
  value '${FEATURES} parallel-fetch getbinpkg'
end

portage_make_conf_entry "PORTAGE_BINHOST" do
  value 'http://www.example.com'
end

portage_make_conf_entry "PORTAGE_BINHOST" do
  action :remove
end
