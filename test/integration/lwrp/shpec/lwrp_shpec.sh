describe "portage lwrp"
  describe "use"
    it "should add entries"
      grep "sys-kernel/genkernel -cryptsetup" /etc/portage/package.use
      assert equal "$?" 0

    it "should remove entries"
      grep "sys-kernel/gentoo-sources symlink" /etc/portage/package.use
      assert equal "$?" 1
  end_describe

  describe "mask"
    it "should add entries"
      grep ">=app-admin/chef-0.10.24" /etc/portage/package.mask
      assert equal "$?" 0

    it "should remove entries"
      grep ">=net-libs/nodejs-0.9.0" /etc/portage/package.mask
      assert equal "$?" 1
  end_describe

  describe "keywords"
    it "should add entries"
      grep "dev-db/mysql ~amd64" /etc/portage/package.keywords
      assert equal "$?" 0

    it "should remove entries"
      grep "dev-vcs/git ~amd64" /etc/portage/package.keywords
      assert equal "$?" 1
  end_describe

  describe "make_conf_entry"
    it "should add entries"
      grep 'FEATURES="${FEATURES} parallel-fetch getbinpkg"' /etc/portage/make.conf
      assert equal "$?" 0

    it "should remove entries"
      grep 'PORTAGE_BINHOST="http://www.example.com"' /etc/portage/make.conf
      assert equal "$?" 1
  end_describe
end_describe
